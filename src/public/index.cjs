fetch("./output/1-matches-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    let dataToPlot = Object.values(data);

    Highcharts.chart("container1", {
      title: {
        text: "Number of matches played per year for all the years in IPL.",
        align: "left",
      },

      subtitle: {
        text: 'Source: <a href="https://irecusa.org/programs/solar-jobs-census/" target="_blank">IREC</a>',
        align: "left",
      },

      yAxis: {
        title: {
          text: "Number of matches per year",
        },
      },

      xAxis: {
        accessibility: {
          rangeDescription: "Range: 2010 to 2020",
        },
      },

      legend: {
        layout: "vertical",
        align: "right",
        verticalAlign: "middle",
      },

      plotOptions: {
        series: {
          label: {
            connectorAllowed: false,
          },
          pointStart: 2008,
        },
      },

      series: [
        {
          name: "Matches per year",
          data: dataToPlot,
        },
      ],

      responsive: {
        rules: [
          {
            condition: {
              maxWidth: 500,
            },
            chartOptions: {
              legend: {
                layout: "horizontal",
                align: "center",
                verticalAlign: "bottom",
              },
            },
          },
        ],
      },
    });
  });

// 2nd question

fetch("./output/2-matches-won-per-team-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    let playerNames = Object.keys(data);

    const newData = playerNames.map((each) => {
      let allYears = {
        2008: 0,
        2009: 0,
        2010: 0,
        2011: 0,
        2012: 0,
        2013: 0,
        2014: 0,
        2015: 0,
        2016: 0,
        2017: 0,
      };

      let changedData = Object.assign(allYears, data[each]);
      let newSeries = {
        name: each,
        data: Object.values(changedData),
      };
      return newSeries;
    });
    console.log(newData);

    Highcharts.chart("container2", {
      chart: {
        type: "column",
      },
      title: {
        text: "Number of matches won per team per year in IPL.",
      },
      subtitle: {
        text: "Data: https://www.kaggle.com/manasgarg/ipl",
      },
      xAxis: {
        categories: [
          "2008",
          "2009",
          "2010",
          "2011",
          "2012",
          "2013",
          "2014",
          "2015",
          "2016",
          "2017",
        ],
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Number of times matches won",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.4,
          borderWidth: 0,
        },
      },
      series: newData,
    });
  });

fetch("./output/3-extra-runs-per-team-2016.json")
  .then((data) => data.json())
  .then((data) => {
    let playerNames = Object.keys(data);
    let matchesCount = Object.values(data);

    Highcharts.chart("container3", {
      chart: {
        type: "column",
      },
      title: {
        text: "Extra runs conceded per team in the year 2016",
      },
      xAxis: {
        categories: playerNames,
      },
      credits: {
        enabled: false,
      },
      series: [
        {
          name: "Extra runs",
          data: matchesCount,
        },
      ],
    });
  });

fetch("./output/4-top-10-economical-bowlers-2015.json")
  .then((data) => data.json())
  .then((data) => {
    let playerNames = [];
    let matchesCount = [];
    data.map((each) => {
      playerNames.push(each[0]);
      economy = each[1];
      matchesCount.push(economy.economy_rate);
    });

    const new_data = Highcharts.chart("container4", {
      chart: {
        type: "column",
      },
      title: {
        text: "The number of times each team won the toss and also won the match",
      },
      xAxis: {
        categories: [
          "2008",
          "2009",
          "2010",
          "2011",
          "2012",
          "2013",
          "2014",
          "2015",
          "2016",
          "2017",
        ],
      },
      credits: {
        enabled: false,
      },
      series: [
        {
          data: matchesCount,
        },
      ],
    });
  });

fetch("./output/5-won-toss-and-won-match.json")
  .then((data) => data.json())
  .then((data) => {
    let playerNames = [];
    let matchesCount = [];
    playerNames = Object.keys(data);
    matchesCount = Object.values(data);

    Highcharts.chart("container5", {
      chart: {
        type: "column",
      },
      title: {
        text: "The number of times each team won the toss and also won the match",
      },
      xAxis: {
        categories: playerNames,
      },
      credits: {
        enabled: false,
      },
      series: [
        {
          name: "Match",
          data: matchesCount,
        },
      ],
    });
  });

fetch("./output/6-player-of-the-match-for-season.json")
  .then((data) => data.json())
  .then((data) => {
    const allYears = Object.keys(data);
    const playerList = [];
    const manOfMatch = [];
    const players = Object.keys(data).map((each) => {
      playerList.push(data[each][0].player);
      manOfMatch.push(data[each][0].number_of_times);
    });

    let newArray = allYears.map((each, index) => {
      return playerList[index].concat(each);
    });

    Highcharts.chart("container6", {
      chart: {
        type: "column",
      },
      title: {
        text: "Number of matches won per team per year in IPL.",
      },
      subtitle: {
        text: "Data: https://www.kaggle.com/manasgarg/ipl",
      },
      xAxis: {
        categories: newArray,
        crosshair: true,
      },

      yAxis: {
        min: 0,
        title: {
          text: "Number of times matches won",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.4,
          borderWidth: 0,
        },
        pointStart: 2008,
      },
      series: [
        {
          data: manOfMatch,
        },
      ],
    });
  });

fetch("./output/7-batsman-strike-rate-per-season.json")
  .then((data) => data.json())
  .then((data) => {
    let allPlayers = Object.keys(data);
    let years = [
      "2008",
      "2009",
      "2010",
      "2011",
      "2012",
      "2013",
      "2014",
      "2015",
      "2016",
      "2017",
    ];
    const newArray = {};
    console.log(allPlayers.length);
    const newData = years.map((eachYear) => {
      allPlayers.map((eachPlayer) => {
        let yearsPlayers = Object.keys(data[eachPlayer]);
        newArray[eachYear] ?? (newArray[eachYear] = []);
        if (yearsPlayers.includes(eachYear)) {
          newArray[eachYear].push(data[eachPlayer][eachYear].strikeRate);
        } else {
          newArray[eachYear].push(0);
        }
      });
    });
    let dataToshow = Object.keys(newArray).map((eachYear) => {
      let newSeries = {
        name: eachYear,
        data: newArray[eachYear],
      };
      return newSeries;
    });
    console.log(dataToshow);
    Highcharts.chart("container7", {
      chart: {
        type: "bar",
      },
      title: {
        text: "The strike rate of a batsman for each season",
        align: "left",
      },
      subtitle: {
        text:
          "Source: <a " +
          'href="https://en.wikipedia.org/wiki/List_of_continents_and_continental_subregions_by_population"' +
          'target="_blank">Wikipedia.org</a>',
        align: "left",
      },
      xAxis: {
        categories: allPlayers,
        title: {
          text: null,
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: "Strike rate",
          align: "high",
        },
        labels: {
          overflow: "allPlayers",
        },
      },
      tooltip: {
        valueSuffix: "",
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true,
          },
        },
      },
      legend: {
        layout: "vertical",
        align: "right",
        verticalAlign: "top",
        x: -80,
        y: 160,
        floating: true,
        borderWidth: 1,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || "#FFFFFF",
        shadow: true,
      },
      credits: {
        enabled: false,
      },
      series: dataToshow,
    });
  });

fetch("./output/8-player-dismissed.json")
  .then((data) => data.json())
  .then((data) => {
    Highcharts.chart("container8", {
      chart: {
        type: "column",
      },
      title: {
        text: "The highest number of times one player has been dismissed by another player",
      },
      xAxis: {
        categories: [Object.keys(data)],
      },
      credits: {
        enabled: false,
      },
      series: [
        {
          name: "Dismissed by Z Khan",
          data: [Object.values(data)[0]["Z Khan"]],
        },
      ],
    });
  });

fetch("./output/9-super-over-economy-bowler.json")
  .then((data) => data.json())
  .then((data) => {
    console.log(data);
    Highcharts.chart("container9", {
      chart: {
        type: "column",
      },
      title: {
        text: "The bowler with the best economy in super overs",
      },
      xAxis: {
        categories: [data[0][0]],
      },
      credits: {
        enabled: true,
      },
      series: [
        {
          name: "Economy rate of bowler",
          data: [data[0][1].economy_rate],
        },
      ],
    });
  });
