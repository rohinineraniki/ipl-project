const csv_matches_path = "src/data/matches.csv";
const csv_deliveries_path = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");

const fs = require("fs");

function top_10_ecomonical_bowlers_2015(matches_json, deliveries_json, year) {
  if (
    matches_json === undefined ||
    deliveries_json === undefined ||
    !Array.isArray(matches_json) ||
    !Array.isArray(deliveries_json)
  ) {
    return {};
  } else {
    let match_id_array = [];
    let bowler_total_runs = {};
    for (let each_match of matches_json) {
      if (parseInt(each_match.season) === year) {
        match_id_array.push(each_match.id);
      }
    }

    for (let each_match of deliveries_json) {
      for (let match_id of match_id_array) {
        if (parseInt(each_match.match_id) === parseInt(match_id)) {
          if (each_match.bowler in bowler_total_runs) {
            bowler_total_runs[each_match.bowler].total_runs += parseInt(
              each_match.total_runs
            );

            bowler_total_runs[each_match.bowler].total_bye_runs += parseInt(
              each_match.bye_runs
            );
            bowler_total_runs[each_match.bowler].total_legbye_runs += parseInt(
              each_match.legbye_runs
            );

            if (
              each_match.wide_runs === "0" &&
              each_match.noball_runs === "0"
            ) {
              bowler_total_runs[each_match.bowler].total_balls += 1;
            }
          } else {
            if (
              each_match.wide_runs === "0" &&
              each_match.noball_runs === "0"
            ) {
              bowler_total_runs[each_match.bowler] = {
                total_runs: parseInt(each_match.total_runs),
                total_balls: 1,
                total_bye_runs: parseInt(each_match.bye_runs),
                total_legbye_runs: parseInt(each_match.legbye_runs),
              };
            } else {
              bowler_total_runs[each_match.bowler] = {
                total_runs: parseInt(each_match.total_runs),
                total_balls: 0,
                total_bye_runs: parseInt(each_match.bye_runs),
                total_legbye_runs: parseInt(each_match.legbye_runs),
              };
            }
          }
        }
      }
    }
    console.log(bowler_total_runs);
    for (let each_bowler in bowler_total_runs) {
      total_overs =
        parseInt(bowler_total_runs[each_bowler].total_balls / 6) +
        (bowler_total_runs[each_bowler].total_balls % 6) / 6;
      total_runs_for_economy =
        bowler_total_runs[each_bowler].total_runs -
        (bowler_total_runs[each_bowler].total_bye_runs +
          bowler_total_runs[each_bowler].total_legbye_runs);
      economy_rate = total_runs_for_economy / total_overs;
      bowler_total_runs[each_bowler]["total_overs"] = total_overs;
      bowler_total_runs[each_bowler]["economy_rate"] = economy_rate;
    }
    const bowlerSortedByEconomicRate = Object.entries(bowler_total_runs).sort(
      (
        [playerA, { economy_rate: playerAEconomy }],
        [playerB, { economy_rate: playerBEconomy }]
      ) => {
        return playerAEconomy > playerBEconomy ? 1 : -1;
      }
    );

    let bowler_sorted_by_economic_rate_top_10 =
      bowlerSortedByEconomicRate.slice(0, 10);
    return bowler_sorted_by_economic_rate_top_10;
  }
}
csvtojson()
  .fromFile(csv_deliveries_path)
  .then((deliveries_json) => {
    csvtojson()
      .fromFile(csv_matches_path)
      .then((matches_json) => {
        const bowler_sorted_by_economic_rate_top_10 =
          top_10_ecomonical_bowlers_2015(matches_json, deliveries_json, 2015);
        fs.writeFileSync(
          "src/public/output/4-top-10-economical-bowlers-2015.json",
          JSON.stringify(bowler_sorted_by_economic_rate_top_10),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
