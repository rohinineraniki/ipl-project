const csv_matches_path = "src/data/matches.csv";
const csv_deliveries_path = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function extra_runs_per_team_2016(matches_json, deliveries_json, year) {
  if (
    matches_json === undefined ||
    deliveries_json === undefined ||
    !Array.isArray(matches_json) ||
    !Array.isArray(deliveries_json)
  ) {
    return {};
  } else {
    let match_ids_array = [];
    let extra_runs_per_team_2016_result = {};
    for (let each_match of matches_json) {
      if (parseInt(each_match.season) === year) {
        match_ids_array.push(each_match.id);
      }
    }
    for (let match_id of match_ids_array) {
      for (let each_match of deliveries_json) {
        if (parseInt(match_id) === parseInt(each_match.match_id)) {
          if (each_match.bowling_team in extra_runs_per_team_2016_result) {
            extra_runs_per_team_2016_result[each_match.bowling_team] +=
              parseInt(each_match.extra_runs);
          } else {
            extra_runs_per_team_2016_result[each_match.bowling_team] = parseInt(
              each_match.extra_runs
            );
          }
        }
      }
    }
    return extra_runs_per_team_2016_result;
  }
}
csvtojson()
  .fromFile(csv_deliveries_path)
  .then((deliveries_json) => {
    csvtojson()
      .fromFile(csv_matches_path)
      .then((matches_json) => {
        const extra_runs_per_team_2016_result = extra_runs_per_team_2016(
          matches_json,
          deliveries_json,
          2016
        );
        fs.writeFileSync(
          "src/public/output/3-extra-runs-per-team-2016..json",
          JSON.stringify(extra_runs_per_team_2016_result),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
