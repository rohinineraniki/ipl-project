const csv_matches_path = "src/data/matches.csv";
const csv_deliveries_path = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

let matches_per_year_object = {};

function matches_per_year(matches_json) {
  if (matches_json === undefined || !Array.isArray(matches_json)) {
    return {};
  } else {
    for (let each_match of matches_json) {
      if (each_match.season in matches_per_year_object) {
        matches_per_year_object[each_match.season] += 1;
      } else {
        matches_per_year_object[each_match.season] = 1;
      }
    }
    return matches_per_year_object;
  }
}
csvtojson()
  .fromFile(csv_deliveries_path)
  .then((deliveries_json) => {
    csvtojson()
      .fromFile(csv_matches_path)
      .then((matches_json) => {
        const matches_per_year_result = matches_per_year(matches_json);
        fs.writeFileSync(
          "src/public/output/1-matches-per-year.json",
          JSON.stringify(matches_per_year_result),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });

module.exports = matches_per_year;
