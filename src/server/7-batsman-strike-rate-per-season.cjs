const csv_matches_path = "src/data/matches.csv";
const csv_deliveries_path = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function bats_man_strike_rate_per_season(matches_json, deliveries_json) {
  if (
    matches_json === undefined ||
    deliveries_json === undefined ||
    !Array.isArray(matches_json) ||
    !Array.isArray(deliveries_json)
  ) {
    return {};
  } else {
    let matched_ids = {};
    for (let each_match of matches_json) {
      matched_ids[each_match.id] = each_match.season;
    }

    let batsman_run_details_per_year = deliveries_json.reduce(
      (batsman_run_details_per_year, each_match) => {
        const year = matched_ids[each_match.match_id];
        batsman_run_details_per_year[each_match.batsman] ??
          (batsman_run_details_per_year[each_match.batsman] = {});
        batsman_run_details_per_year[each_match.batsman][year] ??
          (batsman_run_details_per_year[each_match.batsman][year] = {
            total_runs: 0,
            total_balls: 0,
          });
        batsman_run_details_per_year[each_match.batsman][year]["total_runs"] +=
          isNaN(each_match.total_runs) ? 0 : Number(each_match.total_runs);
        if (each_match.wide_runs === "0" && each_match.noball_runs === "0") {
          batsman_run_details_per_year[each_match.batsman][year][
            "total_balls"
          ]++;
        }

        return batsman_run_details_per_year;
      },
      {}
    );
    //console.log(batsman_run_details_per_year);

    const batsman_run_details_per_year_obj = Object.keys(
      batsman_run_details_per_year
    ).reduce((acc, player) => {
      Object.keys(batsman_run_details_per_year[player]).reduce((acc1, year) => {
        const totalRuns =
          batsman_run_details_per_year[player][year]["total_runs"];
        const totalBalls =
          batsman_run_details_per_year[player][year]["total_balls"];

        let strikeRate = Number(((totalRuns / totalBalls) * 100).toFixed(2));
        batsman_run_details_per_year[player][year] = { strikeRate: strikeRate };
        return batsman_run_details_per_year;
      }, {});
      return batsman_run_details_per_year;
    }, {});

    console.log(batsman_run_details_per_year_obj);

    return batsman_run_details_per_year_obj;
  }
}
csvtojson()
  .fromFile(csv_deliveries_path)
  .then((deliveries_json) => {
    csvtojson()
      .fromFile(csv_matches_path)
      .then((matches_json) => {
        const bats_man_strike_rate_per_season_batsman_run_details_per_year =
          bats_man_strike_rate_per_season(matches_json, deliveries_json);
        fs.writeFileSync(
          "src/public/output/7-batsman-strike-rate-per-season.json",
          JSON.stringify(
            bats_man_strike_rate_per_season_batsman_run_details_per_year
          ),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
