const csv_matches_path = "src/data/matches.csv";
const csv_deliveries_path = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function matches_won_per_team_per_year(matches_json) {
  if (matches_json === undefined || !Array.isArray(matches_json)) {
    return {};
  } else {
    let matches_won_per_team_per_year_object = {};
    for (let each_match of matches_json) {
      if (each_match.winner) {
        let winner_key = each_match.winner;
        let season_key = each_match.season;

        if (winner_key in matches_won_per_team_per_year_object) {
          if (season_key in matches_won_per_team_per_year_object[winner_key]) {
            matches_won_per_team_per_year_object[winner_key][season_key] += 1;
          } else {
            matches_won_per_team_per_year_object[winner_key][season_key] = 1;
          }
        } else {
          matches_won_per_team_per_year_object[winner_key] = {
            [season_key]: 1,
          };
        }
      }
    }
    return matches_won_per_team_per_year_object;
  }
}
csvtojson()
  .fromFile(csv_deliveries_path)
  .then((deliveries_json) => {
    csvtojson()
      .fromFile(csv_matches_path)
      .then((matches_json) => {
        const matches_won_per_team_per_year_result =
          matches_won_per_team_per_year(matches_json);
        fs.writeFileSync(
          "src/public/output/2-matches-won-per-team-per-year.json",
          JSON.stringify(matches_won_per_team_per_year_result),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
