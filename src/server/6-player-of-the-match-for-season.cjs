const csv_matches_path = "src/data/matches.csv";
const csv_deliveries_path = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function player_of_the_match_for_season(matches_json) {
  if (matches_json === undefined || !Array.isArray(matches_json)) {
    return {};
  } else {
    let player_of_the_match_for_season_result = {};
    for (let each_match of matches_json) {
      let season_key = each_match.season;
      let player_of_match_key = each_match.player_of_match;
      if (season_key in player_of_the_match_for_season_result) {
        if (
          player_of_match_key in
          player_of_the_match_for_season_result[season_key]
        ) {
          player_of_the_match_for_season_result[season_key][
            player_of_match_key
          ] += 1;
        } else {
          if (player_of_match_key !== "") {
            player_of_the_match_for_season_result[season_key][
              player_of_match_key
            ] = 1;
          }
        }
      } else {
        player_of_the_match_for_season_result[season_key] = {
          [player_of_match_key]: 1,
        };
      }
    }
    console.log(player_of_the_match_for_season_result);

    const awards_result = Object.entries(
      player_of_the_match_for_season_result
    ).reduce((player_of_match, [season, player_of_match_with_count]) => {
      const counts = Object.values(player_of_match_with_count);
      const highest_count = Math.max(...counts);

      player_of_match[season] = Object.entries(player_of_match_with_count)
        .filter(([player, count]) => {
          return count === highest_count;
        })
        .map(([player, count]) => {
          return { player: player, number_of_times: count };
        });
      return player_of_match;
    }, {});
    return awards_result;
  }
}
csvtojson()
  .fromFile(csv_deliveries_path)
  .then((deliveriesJson) => {
    csvtojson()
      .fromFile(csv_matches_path)
      .then((matches_json) => {
        const player_of_the_match_for_season_result =
          player_of_the_match_for_season(matches_json, deliveriesJson);
        fs.writeFileSync(
          "src/public/output/6-player-of-the-match-for-season.json",
          JSON.stringify(player_of_the_match_for_season_result),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });

fetch("./output/6-player-of-the-match-for-season.json")
  .then((data) => data.json())
  .then((data) => {
    const allYears = Object.keys(data);
    const playerList = [];
    const manOfMatch = [];
    const players = Object.keys(data).map((each) => {
      playerList.push(data[each][0].player);
      manOfMatch.push(data[each][0].number_of_times);
    });

    let newArray = allYears.map((each, index) => {
      return playerList[index].concat(each);
    });

    Highcharts.chart("container6", {
      chart: {
        type: "column",
      },
      title: {
        text: "Number of matches won per team per year in IPL.",
      },
      subtitle: {
        text: "Data: https://www.kaggle.com/manasgarg/ipl",
      },
      xAxis: {
        categories: newArray,
        crosshair: true,
      },

      yAxis: {
        min: 0,
        title: {
          text: "Number of times matches won",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.4,
          borderWidth: 0,
        },
        pointStart: 2008,
      },
      series: [
        {
          data: manOfMatch,
        },
      ],
    });
  });
