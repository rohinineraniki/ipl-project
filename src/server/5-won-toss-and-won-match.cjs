const csv_matches_path = "src/data/matches.csv";
const csv_deliveries_path = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function won_toss_won_match(matches_json, deliveries_json) {
  if (
    matches_json === undefined ||
    deliveries_json === undefined ||
    !Array.isArray(matches_json) ||
    !Array.isArray(deliveries_json)
  ) {
    return {};
  } else {
    let won_toss_won_match_object = {};
    for (let each_match of matches_json) {
      if (each_match.toss_winner === each_match.winner) {
        if (each_match.winner in won_toss_won_match_object) {
          won_toss_won_match_object[each_match.winner] += 1;
        } else {
          won_toss_won_match_object[each_match.winner] = 1;
        }
      }
    }
    return won_toss_won_match_object;
  }
}
csvtojson()
  .fromFile(csv_deliveries_path)
  .then((deliveries_json) => {
    csvtojson()
      .fromFile(csv_matches_path)
      .then((matches_json) => {
        const won_toss_won_match_result = won_toss_won_match(
          matches_json,
          deliveries_json
        );
        fs.writeFileSync(
          "src/public/output/5-won-toss-and-won-match.json",
          JSON.stringify(won_toss_won_match_result),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
