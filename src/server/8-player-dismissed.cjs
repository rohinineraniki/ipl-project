const csv_matches_path = "src/data/matches.csv";
const csv_deliveries_path = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function player_dismissed(deliveries_json) {
  if (deliveries_json === undefined || !Array.isArray(deliveries_json)) {
    return {};
  } else {
    let dismissed_players = {};

    for (let each_match of deliveries_json) {
      if (each_match.player_dismissed !== "") {
        if (each_match.player_dismissed in dismissed_players) {
          if (each_match.dismissal_kind !== "run out") {
            if (
              each_match.bowler in
              dismissed_players[each_match.player_dismissed]
            ) {
              dismissed_players[each_match.player_dismissed][
                each_match.bowler
              ] += 1;
            } else {
              dismissed_players[each_match.player_dismissed][
                each_match.bowler
              ] = 1;
            }
          } else {
            if (
              each_match.fielder in
              dismissed_players[each_match.player_dismissed]
            ) {
              dismissed_players[each_match.player_dismissed][
                each_match.fielder
              ] += 1;
            } else {
              dismissed_players[each_match.player_dismissed][
                each_match.fielder
              ] = 1;
            }
          }
        } else {
          if (each_match.dismissal_kind !== "run out") {
            dismissed_players[each_match.player_dismissed] = {
              [each_match.bowler]: 1,
            };
          } else {
          }
          dismissed_players[each_match.player_dismissed] = {
            [each_match.fielder]: 1,
          };
        }
      }
    }

    let max_count = [];
    let array_dismissed_players = Object.entries(dismissed_players);
    for (let each_player of array_dismissed_players) {
      let [dismissed_player, bowlers] = each_player;

      let count = Object.values(bowlers);
      let dismissed_by_count = Math.max(...count);
      max_count.push(dismissed_by_count);
    }

    let max_count_value = Math.max(...max_count);
    let dismiss_player_object = {};
    for (let each_player of array_dismissed_players) {
      let [dismissed_player, bowlers] = each_player;

      let obj = Object.entries(bowlers).filter(([player, count]) => {
        if (count === max_count_value) {
          return (dismiss_player_object[dismissed_player] = {
            [player]: count,
          });
        }
      });
    }
    return dismiss_player_object;
  }
}
csvtojson()
  .fromFile(csv_deliveries_path)
  .then((deliveries_json) => {
    csvtojson()
      .fromFile(csv_matches_path)
      .then((matches_json) => {
        const player_dismissed_result = player_dismissed(deliveries_json);
        fs.writeFileSync(
          "src/public/output/8-player-dismissed.json",
          JSON.stringify(player_dismissed_result),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
