const csv_matches_path = "src/data/matches.csv";
const csv_deliveries_path = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function super_over_economy_bowler(deliveries_json) {
  if (deliveries_json === undefined || !Array.isArray(deliveries_json)) {
    return {};
  } else {
    let super_over_object = {};
    for (let each_match of deliveries_json) {
      if (each_match.is_super_over === "1") {
        if (each_match.bowler in super_over_object) {
          super_over_object[each_match.bowler].total_runs += parseInt(
            each_match.total_runs
          );

          super_over_object[each_match.bowler].total_bye_runs += parseInt(
            each_match.bye_runs
          );
          super_over_object[each_match.bowler].total_legbye_runs += parseInt(
            each_match.legbye_runs
          );
          if (each_match.wide_runs === "0" && each_match.noball_runs === "0") {
            super_over_object[each_match.bowler].total_balls += 1;
          }
        } else {
          if (each_match.wide_runs === "0" && each_match.noball_runs === "0") {
            super_over_object[each_match.bowler] = {
              total_runs: parseInt(each_match.total_runs),
              total_balls: 1,
              total_bye_runs: parseInt(each_match.bye_runs),
              total_legbye_runs: parseInt(each_match.legbye_runs),
            };
          } else {
            super_over_object[each_match.bowler] = {
              total_runs: parseInt(each_match.total_runs),
              total_balls: 0,
              total_bye_runs: parseInt(each_match.bye_runs),
              total_legbye_runs: parseInt(each_match.legbye_runs),
            };
          }
        }
      }
    }

    for (let each_bowler in super_over_object) {
      total_overs =
        parseInt(super_over_object[each_bowler].total_balls / 6) +
        (super_over_object[each_bowler].total_balls % 6) / 6;
      total_runs_for_economy =
        super_over_object[each_bowler].total_runs -
        (super_over_object[each_bowler].total_bye_runs +
          super_over_object[each_bowler].total_legbye_runs);
      economy_rate = total_runs_for_economy / total_overs;
      super_over_object[each_bowler]["total_overs"] = total_overs;
      super_over_object[each_bowler]["economy_rate"] = economy_rate;
    }
    const sort_by_economy_rate = Object.entries(super_over_object).sort(
      (
        [playerA, { economy_rate: playerAEconomy }],
        [playerB, { economy_rate: playerBEconomy }]
      ) => {
        return playerAEconomy > playerBEconomy ? 1 : -1;
      }
    );

    let super_over_economy_rate = sort_by_economy_rate.slice(0, 1);
    return super_over_economy_rate;
  }
}
csvtojson()
  .fromFile(csv_deliveries_path)
  .then((deliveries_json) => {
    csvtojson()
      .fromFile(csv_matches_path)
      .then((matches_json) => {
        const super_over_economy_bowler_result =
          super_over_economy_bowler(deliveries_json);
        fs.writeFileSync(
          "src/public/output/9-super-over-economy-bowler.json",
          JSON.stringify(super_over_economy_bowler_result),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
